<?php

namespace AQuery;

class AQuery {
    private $parts;

    /**
     * @param mixed $query Can be either an array of query path segments, any
     *                     Traversable object, or a string.
     * @param string $separator When $query is a string, use this token to
     *                          split the query into parts. Defaults to '/'.
     */
    public function __construct($query, $separator = '/') {
        if (is_array($query)) {
            $this->parts = array_values($query);
        }
        elseif ($query instanceof \Traversable) {
            $this->parts = array();
            foreach ($query as $p) {
                $this->parts[] = $p;
            }
        }
        elseif (is_string($query)) {
            $query = trim($query);
            if (empty($query)) {
                $this->parts = array();
            }
            else {
                $this->parts = explode($separator, $query);
            }
        }
    }

    private static function runS($parts, $obj, $defValue) {
        if (empty($parts)) {
            return $obj;
        }
        if (empty($obj)) {
            return $defValue;
        }
        $part = array_shift($parts);
        if (is_array($obj) || $obj instanceof ArrayAccess) {
            if (isset($obj[$part])) {
                return self::runS($parts, $obj[$part], $defValue);
            }
            else {
                return $defValue;
            }
        }
        if (is_object($obj)) {
            if (isset($obj->$part)) {
                return self::runS($parts, $obj->$part, $defValue);
            }
            else {
                return $defValue;
            }
        }
        return $defValue;
    }

    /**
     * Run the query.
     * @param mixed $obj The object to query. Can be an array, ArrayAccess, or
     *                   plain old object.
     * @param mixed $defValue What to return when the query does not resolve.
     * @return mixed Whatever the query resolves to, or $defValue if it
     *               doesn't.
     */
    public function run($obj, $defValue = null) {
        return self::runS($this->parts, $obj, $defValue);
    }

    /**
     * A static convenience wrapper that executes a query directly, without the
     * need to explicitly create an AQuery object and then call its run()
     * method.
     * If you need to run the same query several times, instantiating a query
     * once and reusing it may be more efficient though.
     * @param mixed $query
     * @param mixed $obj The object to query. Can be an array, ArrayAccess, or
     *                   plain old object.
     * @param mixed $defValue What to return when the query does not resolve.
     * @return mixed Whatever the query resolves to, or $defValue if it
     *               doesn't.
     * @see AQuery::__construct()
     */
    public static function query($query, $obj, $defValue = null) {
        $aquery = new AQuery($query);
        return $aquery->run($obj, $defValue);
    }
};
