<?php

require_once dirname(__FILE__) . '/../src/AQuery/AQuery.php';

use AQuery\AQuery;

class AQueryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider queryProvider
     */
    public function testQuery($query, $object, $defValue, $expected) {
        $aquery = new AQuery($query);
        $actual = $aquery->run($object, $defValue);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider queryProvider
     */
    public function testQueryStatic($query, $object, $defValue, $expected) {
        $actual = AQuery::query($query, $object, $defValue);
        $this->assertEquals($expected, $actual);
    }

    public function queryProvider() {
        return array(
            // empty query returns the query object as-is
            array('', 'foobar', 'def', 'foobar'),
            array('', false, null, false),
            array('', true, null, true),
            // querying into an empty array yields the default value
            array('foo/bar/baz', array(), 'blerch', 'blerch'),
            // querying into an empty object yields the default value
            array('foo/bar/baz', (object)(array()), 'blerch', 'blerch'),
            // let's try a query that resolves to a scalar
            array('foo/bar/baz', array('foo' => array('bar' => array('baz' => 'quux'))), 'blerch', 'quux'),
            // the same, but with some objects interspersed
            array('foo/bar/baz', array('foo' => (object)array('bar' => (object)array('baz' => 'quux'))), 'blerch', 'quux'),
            // query into something more complex
            array('foo/bar/baz',
                array(
                    'ape' => 'monkey',
                    'foo' => array(
                        'boink' => 'nope',
                        'bar' => array(
                            'oy' => 'vey',
                            'baz' => 'quux'))),
                'blerch', 'quux'),
            // a query that traverses only halfway into an object
            array('foo/bar', array('foo' => array('bar' => array('baz' => 'quux'))), 'blerch', array('baz' => 'quux')),
            // query into something more complex that fails to resolve halfway
            array('foo/baz/bar',
                array(
                    'ape' => 'monkey',
                    'foo' => array(
                        'boink' => 'nope',
                        'bar' => array(
                            'oy' => 'vey',
                            'baz' => 'quux'))),
                'blerch', 'blerch'),
            // query into a numerically-indexed array
            array('1', array('foo', 'bar', 'baz'), 'blerch', 'bar'),
            // descend into multiple levels of numeric indexing
            array('1/0/1', array('foo', array(array('boink', 'bar', 'bloop'), 'baz')), 'blerch', 'bar'),
        );
    }
}
